import random
import sys
from collections import deque
from enum import Enum

from PyQt5.QtCore import Qt, QObjectCleanupHandler, pyqtSignal
from PyQt5.QtWidgets import QApplication, \
    QWidget, \
    QMessageBox, \
    QDesktopWidget, \
    QVBoxLayout, \
    QPushButton, \
    QHBoxLayout, \
    QGridLayout, QLabel


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
    YELLOW = 4
    PURPLE = 5


class GameModel:

    def __init__(self):
        self.score = 0
        self.block_score = 0
        self.legend = []
        self.field_size = (10, 10)
        self.max_color = 5
        self.game_field = GameModel.generate_random_field(*self.field_size, self.max_color)
        self.highlighted_cells = {}

    def get_legend(self):
        return [(Color(c), GameModel.get_colored_square_count(Color(c), self.game_field, self.field_size))
                for c in range(1, self.max_color + 1)]

    def get_color(self, r, c):
        return self.game_field[r][c], self.highlighted_cells.get((r, c))

    def bfs(self, start_row, start_col):
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        q = deque()
        visited = {}

        color = self.game_field[start_row][start_col]

        if color:
            q.append((start_row, start_col))
        max_row, max_column = self.field_size
        while q:
            (current_row, current_column) = q.popleft()
            visited[(current_row, current_column)] = 1
            for (shift_row, shift_col) in directions:
                next_row = current_row + shift_row
                next_col = current_column + shift_col
                if 0 <= next_row < max_row \
                        and 0 <= next_col < max_column \
                        and not visited.get((next_row, next_col)) \
                        and self.game_field[next_row][next_col] == color:
                    q.append((next_row, next_col))
        return list(visited.keys())

    def is_game_finished(self):
        directions = [(1, 0), (0, 1)]
        max_row, max_column = self.field_size
        for r in range(max_row):
            for c in range(max_column):
                for (shift_row, shift_col) in directions:
                    next_row = r + shift_row
                    next_col = c + shift_col
                    if 0 <= next_row < max_row \
                            and 0 <= next_col < max_column \
                            and self.game_field[next_row][next_col] == self.game_field[r][c] \
                            and self.game_field[r][c]:
                        return False
        return True

    def get_score(self, cells):
        x = len(cells)
        coeff = [72 / 7, -87 / 4, 77 / 5, -75 / 16, 13 / 16, -1 / 16, 1 / 560]
        x_power = 1
        score = 0
        for c in coeff:
            score = score + x_power * c
            x_power = x_power * x
        return max(int(round(score, 0)), 0)

    def exclude_cells(self, cells):
        self.highlighted_cells.clear()
        if not cells:
            return
        if len(cells) <= 1:
            return
        self.score += self.get_score(cells)
        for (r, c) in cells:
            self.game_field[r][c] = None
        max_row, max_column = self.field_size
        for c in range(max_column):
            record_index = max_row
            r = max_row - 1
            while r >= 0:
                if self.game_field[r][c]:
                    record_index = record_index - 1
                    self.game_field[record_index][c] = self.game_field[r][c]
                r = r - 1
            while record_index > 0:
                record_index = record_index - 1
                self.game_field[record_index][c] = None

        column_record_index = -1
        for c in range(max_column):
            if self.game_field[max_row - 1][c]:
                column_record_index = column_record_index + 1
                for r in range(max_row):
                    self.game_field[r][column_record_index] = self.game_field[r][c]
        column_record_index = column_record_index + 1
        while column_record_index < max_column:
            for r in range(max_row):
                self.game_field[r][column_record_index] = None
            column_record_index = column_record_index + 1

    def set_highlighted(self, cells, must_be_highlighted):
        self.highlighted_cells.clear()
        self.block_score = self.get_score(cells)
        if must_be_highlighted and len(cells) > 1:
            for (r, c) in cells:
                self.highlighted_cells[(r, c)] = 1

    @staticmethod
    def get_colored_square_count(target_color, field, field_size):
        (n_rows, n_cols) = field_size
        count = 0
        for r in range(n_rows):
            for c in range(n_cols):
                if field[r][c] == target_color:
                    count = count + 1
        return count

    @staticmethod
    def generate_random_field(n_rows, n_cols, max_color):
        return [[GameModel.generate_random_color(max_color) for _ in range(n_cols)] for _ in range(n_rows)]

    @staticmethod
    def generate_random_color(max_color):
        return Color(random.randint(1, max_color))


class HoverWidget(QWidget):
    mouseHover = pyqtSignal(bool)

    def __init__(self):
        super().__init__()
        self.setMouseTracking(True)

    def enterEvent(self, event):
        self.mouseHover.emit(True)

    def leaveEvent(self, event):
        self.mouseHover.emit(False)


class Cubic(HoverWidget):

    def __init__(self, get_color, on_click, on_hover):
        super().__init__()
        self.get_color = get_color
        self.on_click = on_click

        self.setMinimumWidth(30)
        self.setMinimumHeight(30)
        self.background_color = self.palette().color(self.backgroundRole())
        if on_hover:
            self.mouseHover.connect(on_hover)
        self.setAutoFillBackground(True)

    def set_color(self, color, highlighted):
        p = self.palette()
        qt_color = self.get_color(color, highlighted)
        p.setColor(self.backgroundRole(), qt_color if qt_color else self.background_color)
        self.setPalette(p)
        self.update()

    def mousePressEvent(self, QMouseEvent):
        if self.on_click:
            self.on_click()


class GameWidget(QWidget):

    def __init__(self, model):
        super().__init__()
        self.model = model
        self.game_field_controls = {}
        self.score_label = QLabel("")
        self.block_score_label = QLabel("")
        self.legend_label = [QLabel("") for _ in range(self.model.max_color)]
        self.init_widget()

    def init_widget(self):
        self.setFixedSize(600, 400)
        self.setWindowTitle('Square game')
        self.center()
        self.create_ui()
        self.update_state_from_model()
        self.show()

    @staticmethod
    def get_qt_color(color, highlighted):
        if color is None:
            return None
        if highlighted:
            return Qt.black
        if color == Color.RED:
            return Qt.red
        if color == Color.BLUE:
            return Qt.blue
        if color == Color.GREEN:
            return Qt.green
        if color == Color.YELLOW:
            return Qt.yellow
        if color == Color.PURPLE:
            return Qt.magenta
        return None

    def clear_layout(self):
        if self.layout():
            QWidget().setLayout(self.layout())
            QObjectCleanupHandler().add(self.layout())

    def update_state_from_model(self):
        model = self.model

        (n_rows, n_cols) = model.field_size
        for row in range(n_rows):
            for column in range(n_cols):
                control = self.game_field_controls.get((row, column))
                tmp = model.get_color(row, column)
                control.set_color(*(tmp))
        self.score_label.setText(str(model.score))
        self.block_score_label.setText(str(model.block_score))
        for ((_, count), index) in zip(model.get_legend(), range(model.max_color)):
            self.legend_label[index].setText(str(count))

    def make_move(self, row, column):
        model = self.model
        cells = model.bfs(row, column)
        if len(cells) > 1:
            model.exclude_cells(cells)
            self.highlight_selected_block(True, row, column)
            self.update_state_from_model()
            if model.is_game_finished():
                QMessageBox.information(self,
                                        'Message',
                                        "Game over!",
                                        QMessageBox.Ok,
                                        QMessageBox.Ok)
                self.restart()

    def highlight_selected_block(self, must_be_highlighted, row, column):
        cells = self.model.bfs(row, column)
        self.model.set_highlighted(cells, must_be_highlighted)
        self.update_state_from_model()

    def game_field_widget(self):
        w = QWidget()
        layout = QGridLayout()
        (n_rows, n_cols) = self.model.field_size
        self.game_field_controls.clear()
        for row in range(n_rows):
            for column in range(n_cols):
                square_widget = self.get_colored_square((lambda r, c:
                                                         (lambda: self.make_move(r, c)))
                                                        (row, column),
                                                        (lambda r, c:
                                                         (lambda enter: self.highlight_selected_block(enter, r, c)))
                                                        (row, column))
                self.game_field_controls[(row, column)] = square_widget
                layout.addWidget(square_widget, row + 1, column + 1)
        layout.setSpacing(0)
        w.setLayout(layout)
        return w

    def clear_layout(self):
        if self.layout() is not None:
            old_layout = self.layout()
            for i in reversed(range(old_layout.count())):
                old_layout.itemAt(i).widget().setParent(None)
            import sip
            sip.delete(old_layout)

    def score_widget(self):
        w = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(QLabel("Score:"))
        layout.addWidget(self.score_label)
        w.setLayout(layout)
        return w

    def block_score_widget(self):
        w = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(QLabel("Block score:"))
        layout.addWidget(self.block_score_label)
        w.setLayout(layout)
        return w

    @staticmethod
    def get_colored_square(on_click=None, on_hover=None):
        return Cubic(GameWidget.get_qt_color, on_click, on_hover)

    def legend_widget(self):
        w = QWidget()
        layout = QGridLayout()
        for i in range(self.model.max_color):
            control = GameWidget.get_colored_square()
            control.set_color(Color(i + 1), False)
            layout.addWidget(control, i + 1, 0)
            layout.addWidget(self.legend_label[i], i + 1, 1)

        w.setLayout(layout)
        return w

    def restart(self):
        self.model = GameModel()
        self.update_state_from_model()
        self.update()

    def side_bar_buttons_widget(self):
        w = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(QPushButton("Free games"))
        restart_button = QPushButton("Restart")
        restart_button.clicked.connect(self.restart)
        layout.addWidget(restart_button)
        w.setLayout(layout)
        return w

    def side_bar_widget(self):
        w = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.score_widget())
        layout.addWidget(self.block_score_widget())
        layout.addWidget(self.legend_widget())
        layout.addWidget(self.side_bar_buttons_widget())
        layout.setSpacing(0)
        w.setLayout(layout)
        w.setFixedWidth(150)
        return w

    def create_ui(self):
        vlayout = QHBoxLayout()
        vlayout.addWidget(self.game_field_widget())
        vlayout.addWidget(self.side_bar_widget())
        self.setLayout(vlayout)

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, event):
        reply = QMessageBox.question(self,
                                     'Message',
                                     "Are you sure to quit?",
                                     QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = GameWidget(GameModel())
    sys.exit(app.exec_())
